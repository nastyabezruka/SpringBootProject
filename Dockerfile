FROM maven:3.8.6-amazoncorretto-17

COPY ./ ./
RUN mvn clean package

CMD ["java", "-jar", "target/scheduler-app-1.0-SNAPSHOT.jar"]
